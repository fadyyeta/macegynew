﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="loadPartnerRev.aspx.cs" Inherits="MACEGNew.loadPartnerRev" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width">
    <title></title>
    <style type="text/css" title="dynamic-css" class="options-output">
        .main-nav, .main-nav.dark {
            background-color: #ffffff;
        }

        .title-wrapper {
            background-color: #fff051;
        }

        .footer-social-links a:hover {
            color: #1c2e2e;
        }

        a.link-to-top {
            color: #fff051;
        }

            a.link-to-top:hover {
                color: #81fcff;
            }

        section.title-section, section.title-section, section.title-section {
            background-color: #ffec2e;
        }

        section.title-section, section.title-section, section.title-section {
            background-color: #ffec2e;
        }

        section.title-section, section.title-section, section.title-section {
            background-color: #ffec2e;
        }
    </style>

    <link rel='stylesheet' id='rhythm-fonts-css' href='https://fonts.googleapis.com/css?family=Dosis%3A300%2C400%2C700%7COpen+Sans%3A400italic%2C700italic%2C400%2C300%2C700&#038;subset=latin' type='text/css' media='all' />
    <link rel='stylesheet' id='bootstrap-css' href='https://www.genuinesolutions.co.uk/wp-content/themes/rhythm/css/bootstrap.min.css' type='text/css' media='all' />
    <link rel='stylesheet' id='rhythm-main-css' href='https://www.genuinesolutions.co.uk/wp-content/themes/rhythm/css/style.css' type='text/css' media='all' />

    <link rel='stylesheet' id='owl-carousel-css' href='https://www.genuinesolutions.co.uk/wp-content/themes/rhythm/css/owl.carousel.css' type='text/css' media='all' />


    <script type='text/javascript' src='https://www.genuinesolutions.co.uk/wp-content/uploads/hummingbird-assets/b7713a8b1a875e010f1b3eb8acd8a466.js'></script>


    <script type='text/javascript' src='https://www.genuinesolutions.co.uk/wp-includes/js/backbone.min.js'></script>




    <script type='text/javascript'>
        /* <![CDATA[ */
        var wpApiSettings = { "root": "https:\/\/www.genuinesolutions.co.uk\/wp-json\/", "nonce": "c9b540c3b7", "versionString": "wp\/v2\/" };
        /* ]]> */
    </script>
    <script type='text/javascript' src='https://www.genuinesolutions.co.uk/wp-content/plugins/cleantalk-spam-protect/js/apbct-public.js'></script>

    <script type='text/javascript' src='https://www.genuinesolutions.co.uk/wp-content/plugins/wp-google-maps/wpgmza_data.js'></script>


</head>
<body>
    <form id="form1" runat="server">
        <div id="ajaxContainer">
            <section class="content-section full-width is-fluid cover no-padding no-margin content-section-5c543ce4231a3">
                <div class="row vc_row-fluid">
                    <div class="wpb_column col-md-12">


                        <section class="page-section bg-dark  bg-scroll fullwidth-slider testimonial-slider" data-background="">


                            <div>
                                <div class="container relative">
                                    <div class="row">
                                        <div class="col-md-8 col-md-offset-2 align-center">
                                            <div class="section-icon  padding-top -20px" style="color: #fff051;">
                                                <span class="icon-quote"></span>
                                            </div>
                                            <h3 class="small-title font-alt" style="color: #fff051; font-size: 24px;">What our partners are saying</h3>
                                            <blockquote class="testimonial" style="color: #1c2e2e;">
                                                <p>&#8220;Verbatim&#8217;s relationship with Genuine Solutions began in later 2013, and has proven to be an overwhelmingly positive experience. We are proud of our relationship with Genuine Solutions and hope they will continue their excellent representation of our brand for many years to come.&#8221;</p>
                                                <footer class="testimonial-author" style="color: #fff051;">Verbatim</footer>
                                            </blockquote>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div>
                                <div class="container relative">
                                    <div class="row">
                                        <div class="col-md-8 col-md-offset-2 align-center">
                                            <div class="section-icon  padding-top -20px" style="color: #fff051;">
                                                <span class="icon-quote"></span>
                                            </div>
                                            <h3 class="small-title font-alt" style="color: #fff051; font-size: 24px;">What our partners are saying</h3>
                                            <blockquote class="testimonial" style="color: #1c2e2e;">
                                                <p>&#8220;We have been working in partnership with Genuine Solutions for four years. In that time, working collaboratively, we have seen effective category growth and a real strive for continuous account development. Genuine Solutions are professional, ambitious and fun to work alongside.&#8221;</p>
                                                <footer class="testimonial-author" style="color: #fff051;">Integral</footer>
                                            </blockquote>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div>
                                <div class="container relative">
                                    <div class="row">
                                        <div class="col-md-8 col-md-offset-2 align-center">
                                            <div class="section-icon  padding-top -20px" style="color: #fff051;">
                                                <span class="icon-quote"></span>
                                            </div>
                                            <h3 class="small-title font-alt" style="color: #fff051; font-size: 24px;">What our partners are saying</h3>
                                            <blockquote class="testimonial" style="color: #1c2e2e;">
                                                <p>“It’s been a pleasure to work Genuine Solutions over the last year and our relationship has grown stronger over this time. They are professional in their approach to market and understand customer needs. We look forward to continuing success in the future.”</p>
                                                <footer class="testimonial-author" style="color: #fff051;">House of Marley</footer>
                                            </blockquote>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div>
                                <div class="container relative">
                                    <div class="row">
                                        <div class="col-md-8 col-md-offset-2 align-center">
                                            <div class="section-icon  padding-top -20px" style="color: #fff051;">
                                                <span class="icon-quote"></span>
                                            </div>
                                            <h3 class="small-title font-alt" style="color: #fff051; font-size: 24px;">What our partners are saying</h3>
                                            <blockquote class="testimonial" style="color: #1c2e2e;">
                                                <p>“We’re very excited to have launched our relationship with Genuine Solutions as our distribution partner. They have quickly proven they are a highly professional outfit with a great team who we are sure will help consolidate LG Mobile’s proposition to the market.”</p>
                                                <footer class="testimonial-author" style="color: #fff051;">LG</footer>
                                            </blockquote>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </section>

                    </div>
                </div>
            </section>

            <script type='text/javascript' src='https://www.genuinesolutions.co.uk/wp-content/themes/rhythm/js/bootstrap.min.js'></script>

            <script type='text/javascript' src='https://www.genuinesolutions.co.uk/wp-content/themes/rhythm/js/all.js'></script>

            <script type='text/javascript' src='https://www.genuinesolutions.co.uk/wp-content/themes/rhythm/js/owl.carousel.min.js'></script>
        </div>
    </form>
</body>
</html>
